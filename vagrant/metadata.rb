name             'platform'
maintainer       'Ross Kaffenberger'
maintainer_email 'ross@challengepost.com'
license          'All rights reserved'
description      'Installs/Configures platform'
long_description 'Installs/Configures platform'
version          '0.1.0'

# provided by custom Berksfile
depends 'phantomjs'
