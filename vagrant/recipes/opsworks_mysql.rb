# https://github.com/aws/opsworks-cookbooks/issues/207
directory '/etc/mysql' do
  owner 'root'
  group 'root'
  mode 0755
  recursive true
end
