name             'vagrant_nginx'
maintainer       'The Authors'
maintainer_email 'you@example.com'
license          'all_rights'
description      'Installs/Configures vagrant_nginx'
long_description 'Installs/Configures vagrant_nginx'
version          '0.1.0'

# provided by opsworks
depends 'nginx'
