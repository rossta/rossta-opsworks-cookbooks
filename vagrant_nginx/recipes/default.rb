server_name = node[:nginx][:server_name]

template "/etc/nginx/sites-available/#{server_name}" do
  source "nginx.conf.erb"
  owner node['nginx']['user']
  group node['nginx']['group']
  mode 0644
  action :create
  variables server_name: server_name
end

link "/etc/nginx/sites-enabled/#{server_name}" do
  to "/etc/nginx/sites-available/#{server_name}"
  link_type :symbolic
  owner node['nginx']['user']
  group node['nginx']['group']
  action :create
end

template "/etc/resolver.conf" do
  source "resolver.conf.erb"
  owner node['nginx']['user']
  group node['nginx']['group']
  mode 0644
  action :create
  variables server_name: server_name, ip: node['nginx']['ip']
end

template "/etc/monit/conf.d/nginx.monitrc" do
  owner "root"
  group "root"
  mode 0644
  source "nginx.monitrc.erb"
  notifies :run, "execute[restart-monit]", :immediately
end

execute "restart-monit" do
  command "initctl reload-configuration"
  command "monit reload"
  action :nothing
end
