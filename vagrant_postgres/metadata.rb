name             'custom_postgres'
maintainer       'The Authors'
maintainer_email 'you@example.com'
license          'all_rights'
description      'Installs/Configures custom_postgres'
long_description 'Installs/Configures custom_postgres'
version          '0.1.0'

depends 'postgresql'
