#
# Cookbook Name:: custom_postgres
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

include_recipe "postgresql"
include_recipe "postgresql::ruby"
