#
# Cookbook Name:: custom_postgres
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

include_recipe "postgresql::server"

template "/etc/monit/conf.d/postgres.monitrc" do
  owner "root"
  group "root"
  mode 0644
  source "postgres.monitrc.erb"
  notifies :run, "execute[restart-monit]", :immediately
end

execute "restart-monit" do
  command "initctl reload-configuration"
  command "monit reload"
  action :nothing
end
