#
# Cookbook Name:: vagrant_unicorn
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.
template "/etc/init.d/unicorn" do
  owner "root"
  group "root"
  mode 0755
  source "unicorn.init.erb"
end

template "/etc/monit/conf.d/unicorn.monitrc" do
  owner "root"
  group "root"
  mode 0644
  source "unicorn.monitrc.erb"
  notifies :run, "execute[restart-monit]", :immediately
end

execute "restart-monit" do
  command "initctl reload-configuration"
  command "monit reload"
  action :nothing
end

